package com.duongtung.base.resource

import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.os.Build
import androidx.core.content.ContextCompat

class BaseResource{
    constructor()
    private constructor(ctx: Context){
        context = ctx
    }

    companion object{
        private lateinit var resource: BaseResource
        private lateinit var context : Context
        val instance : BaseResource
            get() {
                return resource
            }
    }

     fun setContext(ctx: Context) : BaseResource {
        resource = BaseResource(ctx)
        return resource
    }
    fun getString(resId : Int) : String {
        return context.getString(resId)
    }

    fun getImageResource(resId: Int) : Drawable? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            context.getDrawable(resId)
        } else {
            ContextCompat.getDrawable(context,resId)
        }
    }

    fun getColor(resId: Int) : Int? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            context.getColor(resId)
        } else {
            ContextCompat.getColor(context,resId)
        }
    }
    fun getResource() : Resources{
        return context.resources
    }
}