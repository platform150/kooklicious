package com.duongtung.base.ui.base

import androidx.lifecycle.ViewModel


abstract class BaseViewModel : ViewModel()