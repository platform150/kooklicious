package com.duongtung.base

import android.app.Application
import com.duongtung.base.resource.BaseResource

abstract class BaseApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        BaseResource().setContext(applicationContext)
    }
}
