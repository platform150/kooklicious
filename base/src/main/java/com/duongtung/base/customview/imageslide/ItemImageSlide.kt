package com.duongtung.base.customview.imageslide

data class ItemImageSlide(val url : Any?, val title : String, val description : String)