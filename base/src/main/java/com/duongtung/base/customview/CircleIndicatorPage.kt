package com.duongtung.base.customview

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.DrawableRes
import androidx.viewpager2.widget.ViewPager2
import com.duongtung.base.R


class CircleIndicatorPage: LinearLayout {
    private val DEFAULT_INDICATOR_WIDTH = 5
    private var mViewpager: ViewPager2? = null
    private var mIndicatorMargin = -1
    private var mIndicatorWidth = -1
    private var mIndicatorHeight = -1
    private var mIndicatorBackgroundResId: Int = R.drawable.white_radius
    private var mIndicatorUnselectedBackgroundResId: Int = R.drawable.white_radius
    private var mLastPosition = -1

    @JvmOverloads
    constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
    ) : super(context, attrs, defStyleAttr) {
        init(context, attrs!!)
    }

    private fun init(
        context: Context,
        attrs: AttributeSet
    ) {
        handleTypedArray(context, attrs)
        checkIndicatorConfig(context)
    }

    fun setViewPager(viewPager: ViewPager2) {
        mViewpager = viewPager
        if (mViewpager!!.adapter != null) {
            mLastPosition = -1
            createIndicators()
            mViewpager!!.unregisterOnPageChangeCallback(mInternalPageChangeListener)
            mViewpager!!.registerOnPageChangeCallback(mInternalPageChangeListener)
            mInternalPageChangeListener.onPageSelected(mViewpager!!.currentItem)
        }
    }

    fun configureIndicator(
        indicatorWidth: Int,
        indicatorHeight: Int,
        indicatorMargin: Int
    ) {
        configureIndicator(
            indicatorWidth, indicatorHeight, indicatorMargin,
            R.drawable.white_radius, R.drawable.white_radius
        )
    }
    fun configureIndicator(
        indicatorWidth: Int, indicatorHeight: Int, indicatorMargin: Int,
        @DrawableRes indicatorBackgroundId: Int,
        @DrawableRes indicatorUnselectedBackgroundId: Int
    ) {
        mIndicatorWidth = indicatorWidth
        mIndicatorHeight = indicatorHeight
        mIndicatorMargin = indicatorMargin
        mIndicatorBackgroundResId = indicatorBackgroundId
        mIndicatorUnselectedBackgroundResId = indicatorUnselectedBackgroundId
        checkIndicatorConfig(context)
    }
    private fun handleTypedArray(
        context: Context,
        attrs: AttributeSet?
    ) {
        if (attrs == null) {
            return
        }
        val typedArray =
            context.obtainStyledAttributes(attrs, R.styleable.CircleIndicatorPager)
        mIndicatorWidth =
            typedArray.getDimensionPixelSize(R.styleable.CircleIndicatorPager_ci_width, -1)
        mIndicatorHeight =
            typedArray.getDimensionPixelSize(R.styleable.CircleIndicatorPager_ci_height, -1)
        mIndicatorMargin =
            typedArray.getDimensionPixelSize(R.styleable.CircleIndicatorPager_ci_margin, -1)
        mIndicatorBackgroundResId = typedArray.getResourceId(
            R.styleable.CircleIndicatorPager_ci_drawable,
            R.drawable.white_radius
        )
        mIndicatorUnselectedBackgroundResId = typedArray.getResourceId(
            R.styleable.CircleIndicatorPager_ci_drawable_unselected,
            mIndicatorBackgroundResId
        )
        val orientation =
            typedArray.getInt(R.styleable.CircleIndicatorPager_ci_orientation, -1)
        setOrientation(if (orientation == VERTICAL) VERTICAL else HORIZONTAL)
        val gravity = typedArray.getInt(R.styleable.CircleIndicatorPager_ci_gravity, -1)
        setGravity(if (gravity >= 0) gravity else Gravity.CENTER)
        typedArray.recycle()
    }

    private fun checkIndicatorConfig(context: Context) {
        mIndicatorWidth =
            if (mIndicatorWidth < 0) dip2px(DEFAULT_INDICATOR_WIDTH.toFloat()) else mIndicatorWidth
        mIndicatorHeight =
            if (mIndicatorHeight < 0) dip2px(DEFAULT_INDICATOR_WIDTH.toFloat()) else mIndicatorHeight
        mIndicatorMargin =
            if (mIndicatorMargin < 0) dip2px(DEFAULT_INDICATOR_WIDTH.toFloat()) else mIndicatorMargin
        mIndicatorBackgroundResId =
            if (mIndicatorBackgroundResId == 0) R.drawable.white_radius else mIndicatorBackgroundResId
        mIndicatorUnselectedBackgroundResId =
            if (mIndicatorUnselectedBackgroundResId == 0) mIndicatorBackgroundResId else mIndicatorUnselectedBackgroundResId
    }

    private val mInternalPageChangeListener: ViewPager2.OnPageChangeCallback = object :
        ViewPager2.OnPageChangeCallback() {
        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {
        }

        override fun onPageSelected(position: Int) {
            if (mViewpager!!.adapter == null || mViewpager!!.adapter!!.itemCount <= 0) {
                return
            }
            var currentIndicator : View ?= null
            if (mLastPosition >= 0 && getChildAt(mLastPosition).also {
                    currentIndicator = it
                } != null) {
                currentIndicator!!.setBackgroundResource(mIndicatorUnselectedBackgroundResId)
            }
            val selectedIndicator: View? = getChildAt(position)
            selectedIndicator?.setBackgroundResource(mIndicatorBackgroundResId)
            mLastPosition = position
        }

        override fun onPageScrollStateChanged(state: Int) {}
    }

    private fun createIndicators() {
        removeAllViews()
        val count: Int = mViewpager!!.adapter!!.itemCount
        if (count <= 0) {
            return
        }
        val currentItem = mViewpager!!.currentItem
        for (i in 0 until count) {
            if (currentItem == i) {
                addIndicator(mIndicatorBackgroundResId)
            } else {
                addIndicator(mIndicatorUnselectedBackgroundResId)
            }
        }
    }

    private fun addIndicator(@DrawableRes backgroundDrawableId: Int) {
        val Indicator = View(context)
        Indicator.setBackgroundResource(backgroundDrawableId)
        addView(Indicator, mIndicatorWidth, mIndicatorHeight)
        val lp = Indicator.getLayoutParams() as LayoutParams
        lp.leftMargin = mIndicatorMargin
        lp.rightMargin = mIndicatorMargin
        Indicator.setLayoutParams(lp)
    }

    private fun dip2px(dpValue: Float): Int {
        val scale = resources.displayMetrics.density
        return (dpValue * scale + 0.5f).toInt()
    }
}