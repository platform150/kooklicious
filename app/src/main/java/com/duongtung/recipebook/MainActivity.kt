package com.duongtung.recipebook

import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import com.duongtung.base.BaseActivity
import com.duongtung.recipebook.databinding.ActivityMainBinding

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {
    override fun getViewMode()= MainViewModel::class.java

    override fun getLayout()= R.layout.activity_main

    override fun setBindingViewModel() {
        binding.viewModel = viewModel
        viewModel.updateData()
        viewModel.data.observe(this, Observer { data ->{

        }})
    }

    override fun getToolbar() = binding.toolbar
}