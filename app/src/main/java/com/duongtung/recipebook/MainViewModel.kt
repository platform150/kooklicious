package com.duongtung.recipebook

import androidx.lifecycle.MutableLiveData
import com.duongtung.base.ui.base.BaseViewModel

/**
 * Created by Nguyendk on 4/12/2021.
 */
class MainViewModel : BaseViewModel(){
    val data = MutableLiveData<String>();

    fun updateData(){
        data.postValue("update")
    }
}